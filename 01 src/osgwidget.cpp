#include "OsgWidget.h"
#include <osgGA/TrackballManipulator>       /// osgGA::TrackballManipulator
#include <osgViewer/ViewerEventHandlers>    /// osgViewer::StatsHandler
#include <osg/MatrixTransform>              /// osg::Transform
#include <osg/BoundingSphere>               /// osg::BoundingSphere::bound()
#include <osg/ShapeDrawable>                /// osg::ShapeDrawable
#include <QtWidgets>
#include <osg/LineWidth>
#include <osg/BlendColor>   /// osg::BlendColor
#include <osg/BlendFunc>    /// osg::BlendFunc

//#define NDEBUG
#include "readthread.h"
#include "ifkinematicshandle.h"
#include "eventinteraction.h"

#include <QMetaType>    /// 解决信号与槽中传递自定义类型不能识别的bug
#define THREAD_NUM 5    /// 线程池线程数



OsgWidget::OsgWidget(QWidget *parent) : QWidget(parent),
    pWindow(0),
    pViewer(0),
    pGroup(0),
    curIndex(0),
    curJointText(""),
    curObjectText(""),
    pRear(0)

{

    /// 解决信号与槽中传递自定义类型不能识别的bug
    qRegisterMetaType<NodeInfo>("NodeInfo");

    for (int i = 0; i < THREAD_NUM; ++i) {
        readHandle.push_back(new ReadThread());
        threadPool.push_back(new QThread());
    }

    for (int i = 0; i < THREAD_NUM; ++i) {
        /// 将对象readHandle送入多线程中,它的成员函数将自动放入多线程中运行
        readHandle[i]->moveToThread(threadPool[i]);
        connect(readHandle[i],  SIGNAL(sendResult(NodeInfo)), this, SLOT(recvFile(NodeInfo)));
        connect(threadPool[i], SIGNAL(finished()), readHandle[i], SLOT(deleteLater()));

        threadPool[i]->start();
    }

    /// 通过信号与槽来触发多线程
    connect(this, SIGNAL(runNewThread1(QString)), readHandle[0], SLOT(readJoint(QString)));
    connect(this, SIGNAL(runNewThread2(QString)), readHandle[1], SLOT(readJoint(QString)));
    connect(this, SIGNAL(runNewThread3(QString)), readHandle[2], SLOT(readJoint(QString)));
    connect(this, SIGNAL(runNewThread4(QString)), readHandle[3], SLOT(readJoint(QString)));
    connect(this, SIGNAL(runNewThread5(QString)), readHandle[4], SLOT(readJoint(QString)));

    /// 多线程读取完后,开始加载模型到界面中
    connect(this, SIGNAL(readFinished()), this, SLOT(loadFile()));


    initOsg();
    layout();
}





OsgWidget::~OsgWidget()
{

    for (int i = 0; i < THREAD_NUM; ++i) {
        threadPool[i]->quit();
        threadPool[i]->wait();
    }

}


void OsgWidget::layout()
{

}


void OsgWidget::initOsg()
{

    this->pWindow = createGraphicsWindow(0, 0, 640, 480);
    this->pViewer = osg::ref_ptr<osgViewer::Viewer>(new osgViewer::Viewer);
    this->pGroup =  osg::ref_ptr<osg::Group>(new osg::Group);

    const osg::GraphicsContext::Traits* traits = pWindow->getTraits();

    osg::Camera* camera = pViewer->getCamera();
    camera->setGraphicsContext(pWindow);
    camera->setClearColor(osg::Vec4(0.2, 0.2, 0.6, 1.0) );
    camera->setViewport(new osg::Viewport(0, 0, traits->width, traits->height));
    camera->setProjectionMatrixAsPerspective(30.0f,
                                             static_cast<double>(traits->width)/static_cast<double>(traits->height),
                                             1.0f, 10000.0f );

#if 0
    osg::ref_ptr<osg::Node> node = osgDB::readNodeFile("cow.osg");
    pViewer->setSceneData(node);

#else

    /// 构建旋转矩阵: 0代表基座, 1~6代表1~6轴的旋转矩阵节点, 7代表工具的旋转矩阵节点
    for (int i = 0; i < 8; ++i) {
        osg::ref_ptr<osg::MatrixTransform> pMatrixTransform = new osg::MatrixTransform;
        pMatrixTransform->setName(std::to_string(i));
        if (i > 0) {
            jointMatrifromList.back()->addChild(pMatrixTransform);
        }
        jointMatrifromList.push_back(pMatrixTransform);
    }
    pGroup->addChild(jointMatrifromList.front());
    pViewer->setSceneData(pGroup);

#endif

    osg::ref_ptr<osg::Viewport> viewport = new osg::Viewport(0, 0, this->width(), this->height());
    camera->setViewport(viewport);

    pViewer->addEventHandler(new EventInteraction);

#if 1
    // 定义漫游器
    pViewer->setCameraManipulator(new osgGA::TrackballManipulator);
#else
    // 步骤八：设置相机参数
    osg::Vec3d eyeVect3D;
    osg::Vec3d centerVect3D;
    osg::Vec3d upVect3D;
    // 参考点坐标，通常是物体中心
    centerVect3D = {0 ,0 ,0};
    // 相机向上：尤其注意,相机向上的方向在世界坐标中的方向
    upVect3D = osg::Vec3d(0, 0, 1);
    // 视点及相机位置与物理位置分离
    eyeVect3D = osg::Vec3d(0, 1000, 0);
    pViewer->getCamera()->setViewMatrixAsLookAt(eyeVect3D, centerVect3D, upVect3D);
#endif

    // 视口设为单线程（Qt5必须）
    pViewer->setThreadingModel(osgViewer::Viewer::SingleThreaded);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(pWindow->getGLWidget());
    setLayout(layout);

    connect(&timer, SIGNAL(timeout()), this, SLOT(update()));
    timer.start(100);
}



osgQt::GraphicsWindowQt* OsgWidget::createGraphicsWindow( int x, int y, int w, int h )
{
    osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
    traits->windowDecoration = false;
    traits->x = x;
    traits->y = y;
    traits->width = w;
    traits->height = h;
    traits->doubleBuffer = true;
    return new osgQt::GraphicsWindowQt(traits.get());
}



void OsgWidget::setCurrentJointText(QString text)
{

#ifndef NDEBUG
    qDebug() << QString("OsgWidget::setCurrentJointText: ") << text;
#endif

    this->curJointText = text;

}



void OsgWidget::setCurrentObjectText(QString text)
{

#ifndef NDEBUG
    qDebug() << QString("[OsgWidget::setCurrentObjectText]: ") << text;
#endif

    this->curObjectText = text;

}



static int finishSignalCnt = 0;
static int totalLoadCnt = 0;
/// 用于分配到缓存中的位置
static int index = 0;
bool OsgWidget::addjoint(const QString& fileName)
{

#ifndef NDEBUG
    qDebug() << QStringLiteral("OsgWidget::addNode()");
#endif

    /// 4线程加载需要8s
    /// 负载均衡
    static int cnt = 0;
    cnt %= THREAD_NUM;
    switch(cnt) {
        case 0:
            emit runNewThread1(fileName + ',' + (index + '0'));
            break;
        case 1:
            emit runNewThread2(fileName + ',' + (index + '0'));
            break;
        case 2:
            emit runNewThread3(fileName + ',' + (index + '0'));
            break;
        case 3:
            emit runNewThread4(fileName + ',' + (index + '0'));
            break;
        case 4:
            emit runNewThread5(fileName + ',' + (index + '0'));
            break;
        default:
            emit runNewThread1(fileName + ',' + (index + '0'));
    }
    cnt++;
    index++;
    totalLoadCnt++;

    return true;

}



void OsgWidget::recvFile(NodeInfo nodeInfo)
{
#ifndef NDEBUG
    qDebug() << QStringLiteral("OsgWidget::recvFile");
#endif

    nodeInfoBuffer.push_back(nodeInfo);

    /// 判断当前是最后一个读取的节点 发送加载节点的信号
    finishSignalCnt++;
    if (finishSignalCnt == totalLoadCnt) {
        emit readFinished();
        finishSignalCnt = 0;
        totalLoadCnt = 0;
    }

}


static bool cmp(const NodeInfo& nodeInfo1, const NodeInfo& nodeInfo2)
{
    return  nodeInfo1.jointType < nodeInfo2.jointType;
}


/// 现在只能一次性加载全部 一个一个加载会重复加载之前已经加载过的
void OsgWidget::loadFile()
{

    sort(nodeInfoBuffer.begin(), nodeInfoBuffer.end(), &cmp);

    for (auto nodeInfo : nodeInfoBuffer) {

        osg::ref_ptr<osg::Node> pNode = nodeInfo.pNode;
        QString fileName = nodeInfo.name;

        nodesBuffer.push_back(pNode);

        pNode->setName(fileName.toStdString());

        auto iterator = jointMatrifromList.begin();
        for (int i = 0; i < pRear; ++i) iterator++;
        osg::ref_ptr<osg::MatrixTransform> pMatrixTransform = (*iterator);
        pMatrixTransform->addChild(pNode);
        pRear++;

    }

}



bool OsgWidget::addObject(const QString& fileName)
{

#ifndef NDEBUG
    qDebug() << QStringLiteral("OsgWidget::addObject()");
#endif

    osg::ref_ptr<osg::Group> pGroup = pViewer->getSceneData()->asGroup();

    // 创建矩阵移动节点,并将读取的模型加入到节点当中,矩阵移动节点的本质是一个group
    osg::ref_ptr<osg::Node> pNode = osgDB::readNodeFile(fileName.toStdString());
    int cnt = 0;
    int idx = fileName.size() - 1;
    while (idx >= 0 && fileName[idx--] != '/') cnt++;
    pNode->setName(fileName.right(cnt).toStdString());

    osg::ref_ptr<osg::MatrixTransform> pMatrixTransform = new osg::MatrixTransform;
    pMatrixTransform->setName(fileName.right(cnt).toStdString()+".obj");
    pMatrixTransform->addChild(pNode);
    osg::ref_ptr<osg::Group> coorNode = makeCoordinate(250);
    coorNode->setName(fileName.right(cnt).toStdString()+".coor");
    pMatrixTransform->addChild(coorNode);

    pGroup->addChild(pMatrixTransform);

    return true;
}


bool OsgWidget::removeObject()
{

    if (this->pGroup->getNumChildren() == 0 || curObjectText.isEmpty()) {
        return false;
    }

    FindNodeWithName vistor(curObjectText.toStdString()+".obj");
    this->pViewer->getSceneData()->asGroup()->accept(vistor);
    osg::ref_ptr<osg::Node> node = vistor.getNode();
    if (node == nullptr) return true;
    node->getParent(0)->removeChild(node);
    curObjectText = "";
    return true;

}


bool OsgWidget::removeJoint()
{

#ifndef NDEBUG
    qDebug() << "OsgWidget::removeNode()";
#endif

    if (this->pGroup->getNumChildren() == 0 || curJointText.isEmpty()) {
#ifndef NDEBUG
        qDebug() << QStringLiteral("[OsgWidget::remove] 没有删除的节点");
#endif
        return false;
    }

    FindNodeWithName vistor(curJointText.toStdString());
    this->pViewer->getSceneData()->asGroup()->accept(vistor);
    osg::ref_ptr<osg::Node> node = vistor.getNode();
    node->getParent(0)->removeChild(node);
    pRear--;
    curJointText = "";

    return true;

}



static osg::Matrix EigenMatrix_2_osgMatrix(Eigen::Matrix4d eigenMatrix)
{

    osg::Matrix osgMatrix;
    osgMatrix.set(eigenMatrix(0,0), eigenMatrix(0,1), eigenMatrix(0,2), eigenMatrix(0,3),
                  eigenMatrix(1,0), eigenMatrix(1,1), eigenMatrix(1,2), eigenMatrix(1,3),
                  eigenMatrix(2,0), eigenMatrix(2,1), eigenMatrix(2,2), eigenMatrix(2,3),
                  eigenMatrix(3,0), eigenMatrix(3,1), eigenMatrix(3,2), eigenMatrix(3,3));
    return osgMatrix;

}

#if 0
static Eigen::Vector3d transFunc(const Joint& joint, Eigen::Vector3d p)
{

    Eigen::Matrix4d curMatrix = Eigen::Matrix4d::Identity();

    curMatrix *= osg::Matrix::rotate(osg::inDegrees(joint.j1), osg::Z_AXIS);
    curMatrix *= osg::Matrix::rotate(osg::inDegrees(joint.j2), osg::Y_AXIS);
    curMatrix *= osg::Matrix::rotate(osg::inDegrees(joint.j3), osg::Y_AXIS);
    curMatrix *= osg::Matrix::rotate(osg::inDegrees(joint.j4), osg::X_AXIS);
    curMatrix *= osg::Matrix::rotate(osg::inDegrees(joint.j5), osg::Y_AXIS);
    curMatrix *= osg::Matrix::rotate(osg::inDegrees(joint.j6), osg::Z_AXIS);
    /// world_point = world_2_local * local_point;
    /// world_2_local-1 * world_point = local_point;
    Eigen::Vector3d res = point * osg::Matrix::inverse(curMatrix);
    return {res.x(), res.y(), res.z()};

}
#endif



#include "simulatecontrolwidget.h"
QString OsgWidget::assembleTool()
{
    if (curObjectText == "") return "";

    /// 判断是否已经加载工具
    while (jointMatrifromList.back()->getNumChildren() > 0) {
        /// 删除工具转换节点中的所有节点
        jointMatrifromList.back()->removeChildren(0, 1);
    }

#ifndef NDEBUG
    qDebug() << "[OsgWidget::assembleTool]: " << curObjectText;
#endif

    /// 找到选择的工件
    FindNodeWithName vistor(curObjectText.toStdString()+".obj");
    this->pViewer->getSceneData()->asGroup()->accept(vistor);
    osg::ref_ptr<osg::Node> node = vistor.getNode();
    nodesBuffer.push_back(node);

    /// 获取工具转换节点的矩阵
    osg::ref_ptr<osg::MatrixTransform> pMatrixTransform = jointMatrifromList.back();
    pMatrixTransform->setMatrix(osg::Matrixd::identity());
    osg::Matrix curMatrix = pMatrixTransform->getMatrix();


    /// 获取当前各轴角度
    SimulateControlWidget* simulateControlWidget = SimulateControlWidget::getInstance();
    if (!simulateControlWidget) return "";
    Joint curJointValue = simulateControlWidget->getCurJointsAngle();

    /// 计算末端工具矩阵
    IFKinematicsHandle* ifKinehandle = IFKinematicsHandle::getInstance();
    Eigen::Vector3d tcfPosition = ifKinehandle->getPositionInWorld(curJointValue);
    //tcfPosition = transFunc(curJointValue, tcfPosition);

    /// 装载工具
    curMatrix *= osg::Matrix::translate(osg::Vec3d(tcfPosition[0], tcfPosition[1], tcfPosition[2]));
    pMatrixTransform->setMatrix(curMatrix);
    pMatrixTransform->addChild(node);


    /// 删除原来的工件
    FindNodeWithName vistor2(curObjectText.toStdString()+".obj");
    this->pViewer->getSceneData()->asGroup()->accept(vistor2);
    osg::ref_ptr<osg::Node> origal = vistor2.getNode();
    this->pViewer->getSceneData()->asGroup()->removeChild(origal);

    return curObjectText;

}




bool OsgWidget::rotateBySelfWithAngle(const JointType& joint, const double& angle)
{

#ifndef NDEBUG
    //qDebug() << "OsgWidget::rotateBySelfWithAngle";
#endif

    osg::ref_ptr<osg::Group> pGroup = pViewer->getSceneData()->asGroup();

    if(pGroup->getNumChildren() == 0) {

        return false;

    }

    auto iterator = jointMatrifromList.begin();
    for (int i = 0; i < joint; ++i) iterator++;
    osg::ref_ptr<osg::MatrixTransform> pMatrixTransform = *iterator;
    osg::Matrix curMatrix = pMatrixTransform->getMatrix();

    osg::Vec3d offset = getOffset(joint);

    rotateBySelf(joint, curMatrix, offset, angle);

    pMatrixTransform->setMatrix(curMatrix);

    update();

    return true;

}



bool OsgWidget::rotateBySelfToTargetAngle(const JointType& joint, const double& targetAngle)
{

    osg::ref_ptr<osg::Group> pGroup = pViewer->getSceneData()->asGroup();

    if(pGroup->getNumChildren() == 0) {

        return false;

    }

    auto iterator = jointMatrifromList.begin();
    for (int i = 0; i < joint; ++i) iterator++;
    osg::ref_ptr<osg::MatrixTransform> pMatrixTransform = *iterator;
    osg::Matrix curMatrix = pMatrixTransform->getMatrix();

    static std::vector<double> lastAngle(7, 0.0);

    osg::Vec3d offset = getOffset(joint);

    rotateBySelf(joint, curMatrix, offset, targetAngle-lastAngle[joint]);
    lastAngle[joint] = targetAngle;

    /// 设置矩阵变换
    pMatrixTransform->setMatrix(curMatrix);

    update();
    return true;

}





osg::Vec3d OsgWidget::getOffset(const JointType& joint)
{

    osg::Vec3d offset = {0, 0, 0};
    IFKinematicsHandle* ifKinehandle = IFKinematicsHandle::getInstance();
    double d1, a1, a2, a3, d4;
    ifKinehandle->getRobotParameter(d1, a1, a2, a3, d4);

    switch(joint) {
        case BASE:
            offset = {0, 0, 0};
            break;
        case J1:
            offset = {0, 0, d1};
            break;
        case J2:
            offset = {a1,0, d1};
            break;
        case J3:
            offset = {a1, 0, d1+a2};
            break;
        case J4:
            offset = {a1+d4, 0, d1+a2+a3};
            break;
        case J5:
            offset = {a1+d4, 0, d1+a2+a3};
            break;
        case J6:
            offset = {a1+d4, 0, d1+a2+a3};
            break;
        default:
            offset = {0 ,0 ,0};
    }

    return offset;

}



static std::vector<double> step(7, 0);
static std::vector<double> begin(7, 0);
static std::vector<double> end(7, 0);
static bool isResumeMove = true;



bool OsgWidget::moveJ(const Joint& beginJoint, const Joint& endJoint)
{

#ifndef NDEBUG
    qDebug() << "OsgWidget::moveJ()";
#endif

    begin[1] = beginJoint.j1;  end[1] = endJoint.j1;
    begin[2] = beginJoint.j2;  end[2] = endJoint.j2;
    begin[3] = beginJoint.j3;  end[3] = endJoint.j3;
    begin[4] = beginJoint.j4;  end[4] = endJoint.j4;
    begin[5] = beginJoint.j5;  end[5] = endJoint.j5;
    begin[6] = beginJoint.j6;  end[6] = endJoint.j6;

    for (int i = 1; i <= 6; ++i) {
        step[i] = (end[i] - begin[i]) / 100;
    }

#if 0   /// 绘制空移轨迹的阴影
    if (!isResumeMove) {
        connect(&drawTimer, SIGNAL(timeout()), this, SLOT(drawOneShadow()));
        drawTimer.start(1000);
    }
    isResumeMove = false;
#endif

    connect(&While, SIGNAL(timeout()), this, SLOT(moveAllJoint()));
    While.start(20);

    QEventLoop loop;
    connect(this, SIGNAL(isMoveFinished()), &loop, SLOT(quit()));
    loop.exec(QEventLoop::ExcludeUserInputEvents);

    return true;

}




bool OsgWidget::moveLine(const Joint& beginJoint, const Joint& endJoint)
{

#ifndef NDEBUG
    qDebug() << "OsgWidget::moveLine()";
#endif

    begin[1] = beginJoint.j1;  end[1] = endJoint.j1;
    begin[2] = beginJoint.j2;  end[2] = endJoint.j2;
    begin[3] = beginJoint.j3;  end[3] = endJoint.j3;
    begin[4] = beginJoint.j4;  end[4] = endJoint.j4;
    begin[5] = beginJoint.j5;  end[5] = endJoint.j5;
    begin[6] = beginJoint.j6;  end[6] = endJoint.j6;

    for (int i = 1; i <= 6; ++i) {
        step[i] = (end[i] - begin[i]) / 1;
    }

    connect(&While, SIGNAL(timeout()), this, SLOT(moveAllJoint()));
    While.start(10);

    QEventLoop loop;
    connect(this, SIGNAL(isMoveFinished()), &loop, SLOT(quit()));
    loop.exec(QEventLoop::ExcludeUserInputEvents);


    return true;

}



void OsgWidget::moveAllJoint()
{

    if (isEnd()) {
#ifndef NDEBUG
    qDebug() << "isEnd";
#endif
        emit isMoveFinished();
        disconnect(&While, SIGNAL(timeout()), this, SLOT(moveAllJoint()));
        disconnect(&drawTimer, SIGNAL(timeout()), this, SLOT(drawOneShadow()));
        return;
    }

    for (int i = 0; i <= 6; ++i) {
        rotateBySelfWithAngle(static_cast<JointType>(i), step[i]);
        begin[i] += step[i];
    }

}



void OsgWidget::drawOneShadow()
{

#ifndef NDEBUG
    qDebug() << "OsgWidget::drawOneShadow()";
#endif

    /// 缓存的node模型节点
    int index = 0;

    osg::ref_ptr<osg::Group> group = new osg::Group;

    /// 记录上一个转换节点
    osg::Matrix lastMat = (*jointMatrifromList.begin())->getMatrix();

    for (auto pJointMatrixform : jointMatrifromList) {

        /// 从缓存中拷贝模型节点
        /// 因为拷贝当前的模型节点会连带它的父类节点一起拷贝,查看源码没有断开父类的方法,所以采用提前缓存无任何继承关系的node节点
        osg::ref_ptr<osg::Node> copyNode = nullptr;
        if (index < nodesBuffer.size()) {
            copyNode = nodesBuffer[index];
        }

        /// 获取世界坐标系下的旋转矩阵
        osg::Matrix curMat = pJointMatrixform->getMatrix();
        /// 但是我们想让这个坐标系是相对于上一个坐标系, 因此需要映射,所以左乘矩阵
        /// osg矩阵相乘与矩阵理论刚好相反, 这里相当于是左乘
        if (index > 0) {
            curMat = curMat * lastMat;
            lastMat = curMat;
        }

        index++;

        /// 为curMat新建一个转换节点
        osg::ref_ptr<osg::MatrixTransform> copyMatrixTransform = new osg::MatrixTransform(curMat);
        copyMatrixTransform->addChild(copyNode);
        group->addChild(copyMatrixTransform);

    }

    /// 设置透明度
    osg::ref_ptr<osg::StateSet> state = group->getOrCreateStateSet();
    /// 关闭灯光
    state->setMode(GL_LIGHTING,osg::StateAttribute::OFF|osg::StateAttribute::PROTECTED);
    /// 打开混合融合模式
    state->setMode(GL_BLEND,osg::StateAttribute::ON);
    state->setMode(GL_DEPTH_TEST,osg::StateAttribute::ON);
    state->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
    /// 使用BlendFunc实现透明效果
    osg::ref_ptr<osg::BlendColor> bc =new osg::BlendColor(osg::Vec4(1.0,1.0,1.0,0.0));
    osg::ref_ptr<osg::BlendFunc> bf = new osg::BlendFunc();
    state->setAttributeAndModes(bf,osg::StateAttribute::ON);
    state->setAttributeAndModes(bc,osg::StateAttribute::ON);
    bf->setSource(osg::BlendFunc::CONSTANT_ALPHA);
    bf->setDestination(osg::BlendFunc::ONE_MINUS_CONSTANT_ALPHA);
    bc->setConstantColor(osg::Vec4(1, 1, 1, 0.1));

    /// 起个名字到后面好删除它
    group->setName("shadow");
    pViewer->getSceneData()->asGroup()->addChild(group);

}



void OsgWidget::removeShadow()
{

#ifndef NDEBU
    qDebug() << "OsgWidget::removeShadow()";
#endif

    osg::ref_ptr<osg::Group> group = pViewer->getSceneData()->asGroup();
    FindNodeWithName findNodeName("shadow");

    for (int i = 0; i < 10; ++i) {
        group->accept(findNodeName);
        group->removeChild(findNodeName.getNode());
    }

}



bool OsgWidget:: isEnd()
{

    for (int i = 1; i <= 6; ++i) {
        if (abs(begin[i] - end[i]) >= 0.01) {
            return false;
        }
    }

    return true;

}




static bool isMeetLimit(Joint curJointValue)
{
    IFKinematicsHandle* ifKinehandle = IFKinematicsHandle::getInstance();

    /// 获得各关节最大最小值
    QVector<float> Joint_MIN; QVector<float> Joint_MAX;
    ifKinehandle->getRobotJointLimit(Joint_MIN, Joint_MAX);

    QVector<double> jointVal = {
        curJointValue.j1,
        curJointValue.j2,
        curJointValue.j3,
        curJointValue.j4,
        curJointValue.j5,
        curJointValue.j6,
    };

    for (int i = 0; i < 6 ; ++i) {
        if (jointVal[i] < Joint_MIN[i] || jointVal[i] > Joint_MAX[i]) {
#ifndef NDEBUG
        qDebug() << QString("[isMeetLimit] J%1 out of limit").arg(i+1);
#endif
            return true;
        }
    }

    return false;
}


/* 单轴旋转
 * @joint 要旋转的关节
 * @curMatrix 当前关节矩阵
 * @offset 关节轴距原点偏移量
 * @angle 旋转角度
*/
void OsgWidget::rotateBySelf(const JointType& joint, osg::Matrix& curMatrix, const osg::Vec3d& offset, const float& angle)
{
    curMatrix *= osg::Matrix::translate(-offset);

    switch(joint) {
    case BASE:
    case J1:
        curMatrix *= osg::Matrix::rotate(osg::inDegrees(angle), osg::Z_AXIS);
        break;
    case J2:
    case J3:
        curMatrix *= osg::Matrix::rotate(osg::inDegrees(angle), osg::Y_AXIS);
        break;
    case J4:
        curMatrix *= osg::Matrix::rotate(osg::inDegrees(angle), osg::X_AXIS);
        break;
    case J5:
        curMatrix *= osg::Matrix::rotate(osg::inDegrees(angle), osg::Y_AXIS);
        break;
    case J6:
        curMatrix *= osg::Matrix::rotate(osg::inDegrees(angle), osg::Z_AXIS);
        break;
    }

    curMatrix *= osg::Matrix::translate(offset);

}


/*  @return 返回xyz坐标轴的组节点 */
static const float radius = 5;
osg::ref_ptr<osg::Group> OsgWidget::makeCoordinate(const float& len = 150)
{
    // 创建空组节点
    osg::ref_ptr<osg::Group> group = new osg::Group();

    // 精细度类并设置对应精细度 值越小精度也就越小
    osg::ref_ptr<osg::TessellationHints> hits = new osg::TessellationHints;
    hits->setDetailRatio(10.0f);

    // 定义三种颜色
    osg::Vec4 blue = osg::Vec4(0.0f, 0.0f, 1.0f, 1.0f);
    osg::Vec4 red = osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f);
    osg::Vec4 green = osg::Vec4(0.0f, 1.0f, 0.0f, 1.0f);

    // 坐标轴圆心为黑体球体
    osg::ref_ptr<osg::Sphere> sphere = new osg::Sphere(osg::Vec3(0, 0, 0), radius*1.5);
    osg::ref_ptr<osg::ShapeDrawable> sphereDrawable = new osg::ShapeDrawable(sphere.get());
    sphereDrawable->setColor(osg::Vec4f(0.0, 0.0, 0.0, 1.0));
    group->addChild(sphereDrawable);

    for (int i = 0; i < 3; ++i) {
        // 创建几何信息对象 (center,radius,height)
        osg::ref_ptr<osg::Cylinder> cylinder = new osg::Cylinder(osg::Vec3(0.0, 0.0, len/2), radius, len);
        osg::ref_ptr<osg::Cone> cone = new osg::Cone(osg::Vec3(0.0, 0.0, len), radius*1.5, radius*3);
        // 创建几何体,几何信息加入到几何体
        osg::ref_ptr<osg::ShapeDrawable> cylinderDrawable = new osg::ShapeDrawable(cylinder.get());
        osg::ref_ptr<osg::ShapeDrawable> coneDrawable = new osg::ShapeDrawable(cone.get());
        // 创建字体节点
        osg::ref_ptr<osgText::Text> pTextXAuxis = new osgText::Text;
        // 创建转换节点
        osg::ref_ptr<osg::MatrixTransform> axisTransform = new osg::MatrixTransform();
        axisTransform->addChild(cylinderDrawable);
        // 将圆柱圆锥和字体加入转换矩阵节点中
        axisTransform->addChild(coneDrawable);
        axisTransform->addChild(pTextXAuxis);
        // 设置精细度
        cylinderDrawable->setTessellationHints(hits);
        coneDrawable->setTessellationHints(hits);

        // 0->x 1->y 2->z
        axisTransform->setName(QString("%1").arg(i).toStdString());
        Axis axis = static_cast<Axis>(i);
        switch (axis) {
        case X:
            axisTransform->setMatrix(osg::Matrix::rotate(osg::inDegrees(90.0), osg::Y_AXIS));   // 绕Y轴旋转90°
            cylinderDrawable->setColor(red);
            coneDrawable->setColor(red);
            pTextXAuxis->setText(L"X轴");
            axisTransform->addChild(createCircle("0", 0.7*len, red));
            break;
        case Y:
            axisTransform->setMatrix(osg::Matrix::rotate(osg::inDegrees(-90.0), osg::X_AXIS));  // 绕X轴旋转90°
            cylinderDrawable->setColor(green);
            coneDrawable->setColor(green);
            pTextXAuxis->setText(L"Y轴");
            axisTransform->addChild(createCircle("1", 0.7*len, green));
            break;
        case Z:
            axisTransform->setMatrix(osg::Matrix::rotate(osg::inDegrees(90.0), osg::Z_AXIS));   // 绕Z轴旋转90°
            cylinderDrawable->setColor(blue);
            coneDrawable->setColor(blue);
            pTextXAuxis->setText(L"Z轴");
            axisTransform->addChild(createCircle("2", 0.7*len, blue));
            break;
        }
        // 设置字的位置
        pTextXAuxis->setPosition(osg::Vec3(0.0f, 0.0f, len));
        // 设置字一直保持正面朝向屏幕
        pTextXAuxis->setAxisAlignment(osgText::Text::SCREEN);
        pTextXAuxis->setCharacterSize(32);
        // 将其中单个轴节点加入组节点中
        group->addChild(axisTransform.get());
    }
    return group;
}



/*
 * @xMinLimit xMaxLimit 轨迹点中x方向最小值与最大值, 即x坐标刻度范围
 * @splitNum 将坐标轴分割为n段
*/
static const double circleWidth = 2;
osg::ref_ptr<osg::Geode> OsgWidget::createCircle(const std::string name, const float& radius, const osg::Vec4& color)
{
    // 步骤一: 创建用户自定义几何节点
    osg::ref_ptr<osg::Geode> geode = new osg::Geode;

    // 步骤二: 定义几何信息
    osg::ref_ptr<osg::Geometry> geometry = new osg::Geometry;

    // 步骤三: 设置坐标点数组
    osg::ref_ptr<osg::Vec3Array> vex = new osg::Vec3Array;
    geometry->setVertexArray(vex);
    for (float i = 0; i <= 360; i++) {
        vex->push_back(osg::Vec3(radius*sin(i), radius*cos(i), 0));
    }

    // 步骤四: 设置坐标轴颜色数组
    osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
    colors->push_back(color);
    geometry->setColorArray(colors);
    geometry->setColorBinding(osg::Geometry::BIND_OVERALL);

    // 步骤五: 定义绘图方式
    osg::ref_ptr<osg::PrimitiveSet> primitiveSet = new osg::DrawArrays(osg::PrimitiveSet::LINE_STRIP , 0, vex.get()->size());
    geometry->addPrimitiveSet(primitiveSet);

    // 步骤六: 设置线宽
    osg::ref_ptr<osg::LineWidth> lineWidth = new osg::LineWidth(circleWidth);
    geometry->getOrCreateStateSet()->setAttribute(lineWidth, osg::StateAttribute::ON);

    // 步骤七: 添加几何信息到几何节点中
    geode->addDrawable(geometry);
    geode->setName(name);
    return geode;
}



bool OsgWidget::setHideAxisMode(const bool& isOpen)
{
    // 节点在隐藏之后通过vistor遍历就找不到了,所以用一个局部静态变量记录上一个隐藏的节点地址
    static osg::ref_ptr<osg::Node> plastNode = 0;
    FindNodeWithName vistor(curObjectText.toStdString()+".coor");
    qDebug() << curObjectText+".coor";
    this->pViewer->getSceneData()->asGroup()->accept(vistor);
    osg::ref_ptr<osg::Node> pNode = vistor.getNode();
    if (pNode == nullptr) {
        pNode = plastNode;
    }

    if (pNode == nullptr) {
        return false;
    }

    pNode->setNodeMask(isOpen);
    plastNode = pNode;
    return true;
}


static int pathNodeNum = 0;
void OsgWidget::drawLine(Eigen::Vector3d p1, Eigen::Vector3d p2) const
{

#ifndef NDEBUG
    qDebug() << "OsgWidget::drawLine()";
#endif

    osg::ref_ptr<osg::Geode> geode = new osg::Geode;

    /// 创建几何体的精细度
    static osg::ref_ptr<osg::TessellationHints> pHints = new osg::TessellationHints;
    pHints->setDetailRatio(0.5);

    /// 设置胶囊体的半径
    double radius = 2.5f;

    /// 起点指向终点的向量
    osg::Vec3d vec1(0,0,1);
    osg::Vec3d vec2 = {(p2-p1)[0], (p2-p1)[1], (p2-p1)[2]};
    double len = vec2.length();

    /// 使用Eigen库获取向量v1到向量v2的旋转四元数
    Eigen::Vector3d v1{vec1.x(), vec1.y(), vec1.z()};
    Eigen::Vector3d v2{vec2.x(), vec2.y(), vec2.z()};
    Eigen::Quaterniond qRotate = Eigen::Quaterniond::FromTwoVectors(v1, v2);

    /// 转换成OSG库里的四元数
    osg::Quat rotate(qRotate.x(), qRotate.y(), qRotate.z(), qRotate.w());

    /// 创建一个胶囊体对象记录圆柱体信息
    osg::ref_ptr<osg::Capsule> capsule = new osg::Capsule(osg::Vec3d(p1[0], p1[1], p1[2]), radius, len);

    /// 旋转胶囊体
    capsule->setRotation(rotate);

    /// 根据信息创建一个图形渲染对象
    osg::ref_ptr<osg::ShapeDrawable> capsuleDrawable =  new osg::ShapeDrawable(capsule, pHints);
    capsuleDrawable->setColor(osg::Vec4d(1.0, 1.0, 1.0, 0.2));

    geode->addDrawable(capsuleDrawable);
    geode->setName("path");
    pathNodeNum++;
    pViewer->getSceneData()->asGroup()->addChild(geode);

}




void OsgWidget::removePath() const
{
    osg::ref_ptr<osg::Group> group = pViewer->getSceneData()->asGroup();
    FindNodeWithName findNodeWithName("path");

    for (int i = 0; i <= pathNodeNum; ++i) {
        group->accept(findNodeWithName);
        group->removeChild(findNodeWithName.getNode());
    }
    pathNodeNum = 0;
}





/** 画一个几何球体
 * @point 球体的坐标
*/
void OsgWidget::drawPoint(const osg::Vec3d& point)
{

#ifndef NDEBUG
        qDebug() << QStringLiteral("OsgWidget::drawPoint");
#endif

    /// 绘制点的半径大小
    double radius = 5;

    osg::ref_ptr<osg::Geode> geode = new osg::Geode;

    /// 创建专门指明精细度的类osg::TessellationHints，并设置对应精细度
    osg::ref_ptr<osg::TessellationHints> pHints = new osg::TessellationHints;
    pHints->setDetailRatio(0.5);

    /// 绘制几何类型(几何体)
    geode->addDrawable(new osg::ShapeDrawable(new osg::Sphere(point, radius), pHints));

    /// 节点加入根节点中
    pViewer->getSceneData()->asGroup()->addChild(geode);

}



static int customObjectindex = 0;
std::string OsgWidget::buildBox(const std::vector<float>& boxInfo)
{
#ifndef NDEBUG
    qDebug() << QStringLiteral("OsgWidget::buildBox");
#endif

    if (boxInfo.size() > 3 && boxInfo.size() <= 0) return "";

    osg::ref_ptr<osg::Group> pGroup = pViewer->getSceneData()->asGroup();


    // 绘制盒体（立方体、长方体）
    osg::ref_ptr<osg::Geode> pGeode = new osg::Geode;
    osg::ref_ptr<osg::TessellationHints> pHints = new osg::TessellationHints;
    pHints->setDetailRatio(0.5);
    qreal lengthX = boxInfo[0];
    qreal lengthY = boxInfo[1];
    qreal lengthZ = boxInfo[2];
    pGeode->addDrawable(new osg::ShapeDrawable(new osg::Box(osg::Vec3(0, 0, 0), lengthX, lengthY, lengthZ), pHints));

    std::string fileName = "box_" + std::to_string(customObjectindex);
    pGeode->setName(fileName);

    customObjectindex++;

    osg::ref_ptr<osg::MatrixTransform> pMatrixTransform = new osg::MatrixTransform;
    pMatrixTransform->setName(fileName + ".obj");
    pMatrixTransform->addChild(pGeode);

    float maxlen = std::max(boxInfo[0], std::max(boxInfo[1], boxInfo[2]));
    osg::ref_ptr<osg::Group> coorNode = makeCoordinate(maxlen * 1.5);
    coorNode->setName(fileName + ".coor");
    pMatrixTransform->addChild(coorNode);

    pGroup->addChild(pMatrixTransform);

    return fileName;
}



std::string OsgWidget::buildBall(const float& radius)
{
#ifndef NDEBUG
    qDebug() << QStringLiteral("OsgWidget::buildBall");
#endif

    osg::ref_ptr<osg::Group> pGroup = pViewer->getSceneData()->asGroup();

    // 绘制球体
    osg::ref_ptr<osg::Geode> pGeode = new osg::Geode;
    osg::ref_ptr<osg::TessellationHints> pHints = new osg::TessellationHints;
    pHints->setDetailRatio(0.5);
    pGeode->addDrawable(new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(0, 0, 0), radius), pHints));
    std::string fileName = "ball_" + std::to_string(customObjectindex);
    pGeode->setName(fileName);

    customObjectindex++;

    osg::ref_ptr<osg::MatrixTransform> pMatrixTransform = new osg::MatrixTransform;
    pMatrixTransform->setName(fileName + ".obj");
    pMatrixTransform->addChild(pGeode);

    osg::ref_ptr<osg::Group> coorNode = makeCoordinate(radius * 2);
    coorNode->setName(fileName + ".coor");
    pMatrixTransform->addChild(coorNode);

    pGroup->addChild(pMatrixTransform);

    return fileName;
}



std::string OsgWidget::buildCylinder(const std::vector<float>& cylinderInfo)
{
#ifndef NDEBUG
    qDebug() << QStringLiteral("OsgWidget::buildCylinder");
#endif

    if (cylinderInfo.size() > 2 && cylinderInfo.size() <= 0) return "";

    osg::ref_ptr<osg::Group> pGroup = pViewer->getSceneData()->asGroup();


    // 绘制圆柱
    osg::ref_ptr<osg::Geode> pGeode = new osg::Geode;
    osg::ref_ptr<osg::TessellationHints> pHints = new osg::TessellationHints;
    pHints->setDetailRatio(0.5);
    qreal radius = cylinderInfo[0];
    qreal height = cylinderInfo[1];
    pGeode->addDrawable(new osg::ShapeDrawable(new osg::Cylinder(osg::Vec3(0, 0, 0), radius, height), pHints));

    std::string fileName = "cylinder_" + std::to_string(customObjectindex);
    pGeode->setName(fileName);

    customObjectindex++;

    osg::ref_ptr<osg::MatrixTransform> pMatrixTransform = new osg::MatrixTransform;
    pMatrixTransform->setName(fileName + ".obj");
    pMatrixTransform->addChild(pGeode);

    float maxlen = std::max(radius, height);
    osg::ref_ptr<osg::Group> coorNode = makeCoordinate(maxlen * 2);
    coorNode->setName(fileName + ".coor");
    pMatrixTransform->addChild(coorNode);

    pGroup->addChild(pMatrixTransform);

    return fileName;
}
