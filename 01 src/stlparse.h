﻿#ifndef STLPARSE_H
#define STLPARSE_H

#include <QString>
#include <vector>
#include "global.h"

class STLParse
{
public:
    STLParse();

public:
    bool readFile(const QString& fileName);
    std::vector<Triangle> getData() const;
    std::vector<QString> splitString(const QString& line, const char& ch) const;
private:

    void clearData();

private:
    std::vector<Triangle> data;
};

#endif // STLPARSE_H
