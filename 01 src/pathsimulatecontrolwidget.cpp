﻿#include "pathsimulatecontrolwidget.h"
#include "ui_pathsimulatecontrolwidget.h"
#include <QtWidgets>


PathSimulateControlWidget::PathSimulateControlWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PathSimulateControlWidget)
{

    ui->setupUi(this);

    init();

}



PathSimulateControlWidget::~PathSimulateControlWidget()
{

    delete ui;

}


void PathSimulateControlWidget::init()
{

    ui->x_startLineEdit->setText("20");
    ui->y_startLineEdit->setText("0");
    ui->z_startLineEdit->setText("0");

    ui->x_endLineEdit->setText("20");
    ui->y_endLineEdit->setText("100");
    ui->z_endLineEdit->setText("0");

    ui->x_upVectorLineEdit->setText("0");
    ui->y_upVectorLineEdit->setText("0");
    ui->z_upVectorLineEdit->setText("1");

}


void PathSimulateControlWidget::on_loadButton_clicked()
{

    emit loadButtonSignal();

}



void PathSimulateControlWidget::on_deleteButton_clicked()
{

    emit deleteButtonSignal();

}



void PathSimulateControlWidget::addItem(const QString& fileDir)
{

    int cnt = 0;
    int idx = fileDir.size() - 1;
    while (idx >= 0 && fileDir[idx--] != '/') cnt++;
    ui->listWidget->insertItem(ui->listWidget->count(), fileDir.right(cnt));

}



void PathSimulateControlWidget::deleteItem()
{

    int row = ui->listWidget->currentRow();
    ui->listWidget->takeItem(row);

}



void PathSimulateControlWidget::on_listWidget_currentTextChanged(const QString &currentText)
{

#ifndef NDEBUG

    qDebug() << "PathSimulateControlWidget : current text is " + currentText;

#endif

    emit currentTextSignal(currentText);

}



void PathSimulateControlWidget::on_computer_clicked()
{

    /// 获取lineEdit输入的求交面信息
    QVector<double> intersectorInfo;

    /// 求交面底线起点
    intersectorInfo.push_back(ui->x_startLineEdit->text().toFloat());
    intersectorInfo.push_back(ui->y_startLineEdit->text().toFloat());
    intersectorInfo.push_back(ui->z_startLineEdit->text().toFloat());

    /// 求交面底线终点
    intersectorInfo.push_back(ui->x_endLineEdit->text().toFloat());
    intersectorInfo.push_back(ui->y_endLineEdit->text().toFloat());
    intersectorInfo.push_back(ui->z_endLineEdit->text().toFloat());

    /// 求交面发射方向
    intersectorInfo.push_back(ui->x_upVectorLineEdit->text().toFloat());
    intersectorInfo.push_back(ui->y_upVectorLineEdit->text().toFloat());
    intersectorInfo.push_back(ui->z_upVectorLineEdit->text().toFloat());

    emit computerButtonSignal(intersectorInfo);

}
