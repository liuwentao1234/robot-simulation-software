/********************************************************************************
** Form generated from reading UI file 'pathsimulatecontrolwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PATHSIMULATECONTROLWIDGET_H
#define UI_PATHSIMULATECONTROLWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PathSimulateControlWidget
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *pathListBox;
    QListWidget *listWidget;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *loadButton;
    QPushButton *deleteButton;
    QGroupBox *controlBox;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *startPointLabel;
    QLineEdit *x_startLineEdit;
    QLineEdit *y_startLineEdit;
    QLineEdit *z_startLineEdit;
    QHBoxLayout *horizontalLayout_2;
    QLabel *endPointLabel;
    QLineEdit *x_endLineEdit;
    QLineEdit *y_endLineEdit;
    QLineEdit *z_endLineEdit;
    QHBoxLayout *horizontalLayout_3;
    QLabel *upVectorLabel;
    QLineEdit *x_upVectorLineEdit;
    QLineEdit *y_upVectorLineEdit;
    QLineEdit *z_upVectorLineEdit;
    QPushButton *computer;

    void setupUi(QWidget *PathSimulateControlWidget)
    {
        if (PathSimulateControlWidget->objectName().isEmpty())
            PathSimulateControlWidget->setObjectName(QStringLiteral("PathSimulateControlWidget"));
        PathSimulateControlWidget->setEnabled(true);
        PathSimulateControlWidget->resize(316, 551);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(PathSimulateControlWidget->sizePolicy().hasHeightForWidth());
        PathSimulateControlWidget->setSizePolicy(sizePolicy);
        PathSimulateControlWidget->setMinimumSize(QSize(316, 424));
        layoutWidget = new QWidget(PathSimulateControlWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(11, 21, 291, 411));
        verticalLayout_2 = new QVBoxLayout(layoutWidget);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        pathListBox = new QGroupBox(layoutWidget);
        pathListBox->setObjectName(QStringLiteral("pathListBox"));
        pathListBox->setEnabled(true);
        listWidget = new QListWidget(pathListBox);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(10, 20, 271, 141));

        verticalLayout_2->addWidget(pathListBox);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setSizeConstraint(QLayout::SetMinimumSize);
        loadButton = new QPushButton(layoutWidget);
        loadButton->setObjectName(QStringLiteral("loadButton"));

        horizontalLayout_4->addWidget(loadButton);

        deleteButton = new QPushButton(layoutWidget);
        deleteButton->setObjectName(QStringLiteral("deleteButton"));

        horizontalLayout_4->addWidget(deleteButton);


        verticalLayout_2->addLayout(horizontalLayout_4);

        controlBox = new QGroupBox(layoutWidget);
        controlBox->setObjectName(QStringLiteral("controlBox"));
        layoutWidget1 = new QWidget(controlBox);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 30, 261, 131));
        verticalLayout = new QVBoxLayout(layoutWidget1);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        startPointLabel = new QLabel(layoutWidget1);
        startPointLabel->setObjectName(QStringLiteral("startPointLabel"));

        horizontalLayout->addWidget(startPointLabel);

        x_startLineEdit = new QLineEdit(layoutWidget1);
        x_startLineEdit->setObjectName(QStringLiteral("x_startLineEdit"));

        horizontalLayout->addWidget(x_startLineEdit);

        y_startLineEdit = new QLineEdit(layoutWidget1);
        y_startLineEdit->setObjectName(QStringLiteral("y_startLineEdit"));

        horizontalLayout->addWidget(y_startLineEdit);

        z_startLineEdit = new QLineEdit(layoutWidget1);
        z_startLineEdit->setObjectName(QStringLiteral("z_startLineEdit"));

        horizontalLayout->addWidget(z_startLineEdit);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        endPointLabel = new QLabel(layoutWidget1);
        endPointLabel->setObjectName(QStringLiteral("endPointLabel"));

        horizontalLayout_2->addWidget(endPointLabel);

        x_endLineEdit = new QLineEdit(layoutWidget1);
        x_endLineEdit->setObjectName(QStringLiteral("x_endLineEdit"));

        horizontalLayout_2->addWidget(x_endLineEdit);

        y_endLineEdit = new QLineEdit(layoutWidget1);
        y_endLineEdit->setObjectName(QStringLiteral("y_endLineEdit"));

        horizontalLayout_2->addWidget(y_endLineEdit);

        z_endLineEdit = new QLineEdit(layoutWidget1);
        z_endLineEdit->setObjectName(QStringLiteral("z_endLineEdit"));

        horizontalLayout_2->addWidget(z_endLineEdit);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        upVectorLabel = new QLabel(layoutWidget1);
        upVectorLabel->setObjectName(QStringLiteral("upVectorLabel"));

        horizontalLayout_3->addWidget(upVectorLabel);

        x_upVectorLineEdit = new QLineEdit(layoutWidget1);
        x_upVectorLineEdit->setObjectName(QStringLiteral("x_upVectorLineEdit"));

        horizontalLayout_3->addWidget(x_upVectorLineEdit);

        y_upVectorLineEdit = new QLineEdit(layoutWidget1);
        y_upVectorLineEdit->setObjectName(QStringLiteral("y_upVectorLineEdit"));

        horizontalLayout_3->addWidget(y_upVectorLineEdit);

        z_upVectorLineEdit = new QLineEdit(layoutWidget1);
        z_upVectorLineEdit->setObjectName(QStringLiteral("z_upVectorLineEdit"));

        horizontalLayout_3->addWidget(z_upVectorLineEdit);


        verticalLayout->addLayout(horizontalLayout_3);


        verticalLayout_2->addWidget(controlBox);

        computer = new QPushButton(layoutWidget);
        computer->setObjectName(QStringLiteral("computer"));

        verticalLayout_2->addWidget(computer);

        verticalLayout_2->setStretch(0, 4);
        verticalLayout_2->setStretch(1, 1);
        verticalLayout_2->setStretch(2, 4);

        retranslateUi(PathSimulateControlWidget);

        QMetaObject::connectSlotsByName(PathSimulateControlWidget);
    } // setupUi

    void retranslateUi(QWidget *PathSimulateControlWidget)
    {
        PathSimulateControlWidget->setWindowTitle(QApplication::translate("PathSimulateControlWidget", "Form", Q_NULLPTR));
        pathListBox->setTitle(QApplication::translate("PathSimulateControlWidget", "\350\275\250\350\277\271\345\210\227\350\241\250", Q_NULLPTR));
        loadButton->setText(QApplication::translate("PathSimulateControlWidget", "\350\275\275\345\205\245\350\275\250\350\277\271", Q_NULLPTR));
        deleteButton->setText(QApplication::translate("PathSimulateControlWidget", "\345\210\240\351\231\244\350\275\250\350\277\271", Q_NULLPTR));
        controlBox->setTitle(QApplication::translate("PathSimulateControlWidget", "\346\261\202\344\272\244\351\235\242\350\256\276\347\275\256", Q_NULLPTR));
        startPointLabel->setText(QApplication::translate("PathSimulateControlWidget", "\350\265\267\347\202\271\345\235\220\346\240\207:", Q_NULLPTR));
        endPointLabel->setText(QApplication::translate("PathSimulateControlWidget", "\347\273\210\347\202\271\345\235\220\346\240\207:", Q_NULLPTR));
        upVectorLabel->setText(QApplication::translate("PathSimulateControlWidget", "\346\226\271\345\220\221\345\220\221\351\207\217:", Q_NULLPTR));
        computer->setText(QApplication::translate("PathSimulateControlWidget", "\350\256\241\347\256\227", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PathSimulateControlWidget: public Ui_PathSimulateControlWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PATHSIMULATECONTROLWIDGET_H
