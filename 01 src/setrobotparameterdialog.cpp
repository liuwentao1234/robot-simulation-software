﻿#include "setrobotparameterdialog.h"
#include "ui_setrobotparameterdialog.h"
#include <QDebug>
SetRobotParameterDialog::SetRobotParameterDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SetRobotParameterDialog),
    parameters(QVector<float>(26))
{
    ui->setupUi(this);
    this->setWindowTitle(QStringLiteral("机器人DH参数设置"));

    init();

}


void SetRobotParameterDialog::init()
{

    ui->d1->setText("215");
    ui->a1->setText("109");
    ui->a2->setText("250.5");
    ui->a3->setText("115.5");
    ui->d4->setText("239.5");

    ui->theta1LineEdit->setText("0");       /// theta 1~6
    ui->theta2LineEdit->setText("-90");
    ui->theta3LineEdit->setText("0");
    ui->theta4LineEdit->setText("0");
    ui->theta5LineEdit->setText("90");
    ui->theta6LineEdit->setText("0");

    ui->J1_MIN->setText("-90");     /// J1
    ui->J1_MAX->setText("90");     /// J1

    ui->J2_MIN->setText("-135");     /// J2
    ui->J2_MAX->setText("30");     /// J2

    ui->J3_MIN->setText("-135");    /// J3
    ui->J3_MAX->setText("90");    /// J3

    ui->J4_MIN->setText("-135");    /// J4
    ui->J4_MAX->setText("135");    /// J4

    ui->J5_MIN->setText("-135");    /// J5
    ui->J5_MAX->setText("135");    /// J5

    ui->J6_MIN->setText("-180");    /// J6
    ui->J6_MAX->setText("180");    /// J6

    ui->TCF_X_LineEdit->setText("0");
    ui->TCF_Y_LineEdit->setText("0");
    ui->TCF_Z_LineEdit->setText("135");

}



SetRobotParameterDialog::~SetRobotParameterDialog()
{
    delete ui;
}



/// parameters : d1 a1 a2 a3 d4 θ1 θ2 θ3 θ4 θ5 θ6 j1 j1 j2 j2 j3 j3 j4 j4 j5 j5 j6 j6 tcfx tcfy tcfz
///      index : 0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23   24   25
void SetRobotParameterDialog::on_pushButton_clicked()
{

    parameters[0] = ui->d1->text().toFloat();   /// d1
    parameters[1] = ui->a1->text().toFloat();   /// a1
    parameters[2] = ui->a2->text().toFloat();   /// a2
    parameters[3] = ui->a3->text().toFloat();   /// a3
    parameters[4] = ui->d4->text().toFloat();   /// d4

    parameters[5] = ui->theta1LineEdit->text().toFloat();    /// θ1
    parameters[6] = ui->theta2LineEdit->text().toFloat();    /// θ2

    parameters[7] = ui->theta3LineEdit->text().toFloat();    /// θ3
    parameters[8] = ui->theta4LineEdit->text().toFloat();    /// θ4
    parameters[9] = ui->theta5LineEdit->text().toFloat();    /// θ5
    parameters[10] = ui->theta6LineEdit->text().toFloat();    /// θ6

    parameters[11] = ui->J1_MIN->text().toFloat();     /// J1
    parameters[12] = ui->J1_MAX->text().toFloat();     /// J1

    parameters[13] = ui->J2_MIN->text().toFloat();     /// J2
    parameters[14] = ui->J2_MAX->text().toFloat();     /// J2

    parameters[15]  = ui->J3_MIN->text().toFloat();    /// J3
    parameters[16] = ui->J3_MAX->text().toFloat();    /// J3

    parameters[17] = ui->J4_MIN->text().toFloat();    /// J4
    parameters[18] = ui->J4_MAX->text().toFloat();    /// J4

    parameters[19] = ui->J5_MIN->text().toFloat();    /// J5
    parameters[20] = ui->J5_MAX->text().toFloat();    /// J5

    parameters[21] = ui->J6_MIN->text().toFloat();    /// J6
    parameters[22] = ui->J6_MAX->text().toFloat();    /// J6

    parameters[23] = ui->TCF_X_LineEdit->text().toFloat();    /// tcfx
    parameters[24] = ui->TCF_Y_LineEdit->text().toFloat();    /// tcfy
    parameters[25] = ui->TCF_Z_LineEdit->text().toFloat();    /// tcfz


    emit sendParameter(parameters);
    accept();

}

void SetRobotParameterDialog::on_pushButton_2_clicked()
{
    close();
}
