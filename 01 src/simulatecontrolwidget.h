﻿#ifndef SIMULATECONTROLWIDGET_H
#define SIMULATECONTROLWIDGET_H

#include <QWidget>
#include <QSharedPointer>
#include "global.h"         /// Joint

class QPushButton;
class QGridLayout;
class QListWidget;
class QLineEdit;
class QLabel;
class QSlider;
class QSpinBox;
class QRadioButton;


class SimulateControlWidget : public QWidget
{

    Q_OBJECT

    public:

        /** 默认机器人控制窗口只有一个, 懒汉式单例模式*/
        static SimulateControlWidget* createSimulateControlWidget(QWidget *parent) {
            if (!instance) {
                instance = new SimulateControlWidget(parent);
            }
            return instance;
        }

        /** 饿汉式单例 */
        static SimulateControlWidget* getInstance() {
            return (instance ? instance : nullptr);
        }

        virtual ~SimulateControlWidget();

        /** 将新加入的关节或工件插入/删除到列表中*/
        void insertJointItem(const QString& fileName);
        void insertObjectItem(const QString& fileName);
        void deleteJointItem();
        void deleteObjectItem();

        QListWidget* getJointListWidet();
        QListWidget* getObjectListWidet();
        QRadioButton* getRadioButton();

        /** 获取当前各关节的角度值 */
        Joint getCurJointsAngle() const;

        /** 设置当前各关节的角度值 */
        void setCurJointsAngle(Joint joint);

        /** 为了在加载完模型时自动弹出设置机器人参数对话框, 因此将此按钮公开 */
        QSharedPointer<QPushButton> setRobotParameterButton; // 设置机器人参数按钮

    private slots:

        /** 接受设置机器人参数对话框设置的参数 */
        void recvParameters(QVector<float>);


    private:

        SimulateControlWidget(QWidget *parent = nullptr);

        void init();

        void layout();

        void createAction();


    private:

        static SimulateControlWidget* instance;

        QSharedPointer<QGridLayout> gridLayout;              /// 格栅布局
        QSharedPointer<QListWidget> jointListWidget;         /// 关节列表
        QSharedPointer<QListWidget> objectListWidget;        /// 工件列表

        QSharedPointer<QPushButton> loadJointButton;         /// 加载关节按钮
        QSharedPointer<QPushButton> loadObjectButton;        /// 加载工件按钮

        QSharedPointer<QPushButton> removeJointButton;       /// 移除模型按钮
        QSharedPointer<QPushButton> removeObjectButton;

        QSharedPointer<QRadioButton> hideAxis;               /// 显示/隐藏坐标系


        QSharedPointer<QLabel> jointListLabel;
        QSharedPointer<QLabel> objectListLabel;

        QSharedPointer<QPushButton> customObjectButton;      /// 自定义工件参数按钮
        QSharedPointer<QPushButton> assembleToolButton;      /// 装配工具按钮

        std::vector<QSharedPointer<QSpinBox>> jointSpinBox;  /// 基座,轴1~轴6的微调器数组
        std::vector<QSharedPointer<QSlider>> jointSlider;    /// 基座,轴1~轴6的滑块数组
        std::vector<QSharedPointer<QLabel>> jointLabel;      /// 基座,轴1~轴6的滑块数组

};

#endif // SIMULATECONTROLWIDGET_H
