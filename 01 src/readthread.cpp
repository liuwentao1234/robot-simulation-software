﻿#include "readthread.h"
#include <QThread>
#include <QDebug>
#include <osgDB/ReadFile>

ReadThread::ReadThread(QObject *parent) : QObject(parent)
{

}




void ReadThread::readJoint(QString fileName)
{

#ifndef NDEBUG
    qDebug() << "ReadThread::readJoint | the thread num:" << QThread::currentThreadId();
#endif

    /// 提取关节应该放在缓存中的位置下标index
    std::string name = fileName.toStdString();
    int len = name.size();
    int indexNum = 0, cnt = 0;
    for (int i = len-1; i >= 0 && name[i] != ','; i--) {
         indexNum = indexNum * 10 + (name[i] - '0');
         cnt++;
    }

    name.erase(len - cnt - 1);

    /// 读取关节
    osg::ref_ptr<osg::Node> node = osgDB::readNodeFile(name);
    if (node == nullptr) {
#ifndef NDEBUG
    qDebug() << "[ReadThread::readJoint] read mode err";
#endif
        return;
    }

    /// 提取名字
    int left = name.size() - 1, right = name.size() - 1;
    while (name[left] != '/') left--;
    name = name.substr(left + 1, right - left);


    /// 构造返回信息
    NodeInfo nodeInfo;
    nodeInfo.pNode = node;
    nodeInfo.name = QString::fromStdString(name);
    nodeInfo.jointType = static_cast<JointType>(indexNum);

    emit sendResult(nodeInfo);

}
