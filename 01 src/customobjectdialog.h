﻿#ifndef CUSTOMOBJECTDIALOG_H
#define CUSTOMOBJECTDIALOG_H

#include <QDialog>

namespace Ui {
class CustomObjectDialog;
}

class CustomObjectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CustomObjectDialog(QWidget *parent = 0);
    ~CustomObjectDialog();

signals:
    void buildBox(std::vector<float>);
    void buildBall(float);
    void buildCylinder(std::vector<float>);


private slots:
    void on_okButton_clicked();         // 自定义盒体的确认按钮
    void on_cancelButton_clicked();

    void on_okButton_2_clicked();

    void on_cancelButton_2_clicked();

    void on_okButton_3_clicked();

    void on_cancelButton_3_clicked();

private:
    void init();


private:
    Ui::CustomObjectDialog *ui;
};

#endif // CUSTOMOBJECTDIALOG_H
