/****************************************************************************
** Meta object code from reading C++ file 'customobjectdialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../customobjectdialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'customobjectdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CustomObjectDialog_t {
    QByteArrayData data[12];
    char stringdata0[212];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CustomObjectDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CustomObjectDialog_t qt_meta_stringdata_CustomObjectDialog = {
    {
QT_MOC_LITERAL(0, 0, 18), // "CustomObjectDialog"
QT_MOC_LITERAL(1, 19, 8), // "buildBox"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 18), // "std::vector<float>"
QT_MOC_LITERAL(4, 48, 9), // "buildBall"
QT_MOC_LITERAL(5, 58, 13), // "buildCylinder"
QT_MOC_LITERAL(6, 72, 19), // "on_okButton_clicked"
QT_MOC_LITERAL(7, 92, 23), // "on_cancelButton_clicked"
QT_MOC_LITERAL(8, 116, 21), // "on_okButton_2_clicked"
QT_MOC_LITERAL(9, 138, 25), // "on_cancelButton_2_clicked"
QT_MOC_LITERAL(10, 164, 21), // "on_okButton_3_clicked"
QT_MOC_LITERAL(11, 186, 25) // "on_cancelButton_3_clicked"

    },
    "CustomObjectDialog\0buildBox\0\0"
    "std::vector<float>\0buildBall\0buildCylinder\0"
    "on_okButton_clicked\0on_cancelButton_clicked\0"
    "on_okButton_2_clicked\0on_cancelButton_2_clicked\0"
    "on_okButton_3_clicked\0on_cancelButton_3_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CustomObjectDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x06 /* Public */,
       4,    1,   62,    2, 0x06 /* Public */,
       5,    1,   65,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,   68,    2, 0x08 /* Private */,
       7,    0,   69,    2, 0x08 /* Private */,
       8,    0,   70,    2, 0x08 /* Private */,
       9,    0,   71,    2, 0x08 /* Private */,
      10,    0,   72,    2, 0x08 /* Private */,
      11,    0,   73,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, QMetaType::Float,    2,
    QMetaType::Void, 0x80000000 | 3,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CustomObjectDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CustomObjectDialog *_t = static_cast<CustomObjectDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->buildBox((*reinterpret_cast< std::vector<float>(*)>(_a[1]))); break;
        case 1: _t->buildBall((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 2: _t->buildCylinder((*reinterpret_cast< std::vector<float>(*)>(_a[1]))); break;
        case 3: _t->on_okButton_clicked(); break;
        case 4: _t->on_cancelButton_clicked(); break;
        case 5: _t->on_okButton_2_clicked(); break;
        case 6: _t->on_cancelButton_2_clicked(); break;
        case 7: _t->on_okButton_3_clicked(); break;
        case 8: _t->on_cancelButton_3_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (CustomObjectDialog::*_t)(std::vector<float> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CustomObjectDialog::buildBox)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (CustomObjectDialog::*_t)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CustomObjectDialog::buildBall)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (CustomObjectDialog::*_t)(std::vector<float> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CustomObjectDialog::buildCylinder)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject CustomObjectDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_CustomObjectDialog.data,
      qt_meta_data_CustomObjectDialog,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *CustomObjectDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CustomObjectDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CustomObjectDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int CustomObjectDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void CustomObjectDialog::buildBox(std::vector<float> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CustomObjectDialog::buildBall(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void CustomObjectDialog::buildCylinder(std::vector<float> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
