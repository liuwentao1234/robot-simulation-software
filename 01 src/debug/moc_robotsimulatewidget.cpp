/****************************************************************************
** Meta object code from reading C++ file 'robotsimulatewidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../robotsimulatewidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'robotsimulatewidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_RobotSimulateWidget_t {
    QByteArrayData data[27];
    char stringdata0[305];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_RobotSimulateWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_RobotSimulateWidget_t qt_meta_stringdata_RobotSimulateWidget = {
    {
QT_MOC_LITERAL(0, 0, 19), // "RobotSimulateWidget"
QT_MOC_LITERAL(1, 20, 10), // "loadObject"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 9), // "loadJoint"
QT_MOC_LITERAL(4, 42, 12), // "removeObject"
QT_MOC_LITERAL(5, 55, 11), // "removeJoint"
QT_MOC_LITERAL(6, 67, 17), // "setRobotParameter"
QT_MOC_LITERAL(7, 85, 12), // "assembleTool"
QT_MOC_LITERAL(8, 98, 12), // "customObject"
QT_MOC_LITERAL(9, 111, 8), // "buildBox"
QT_MOC_LITERAL(10, 120, 18), // "std::vector<float>"
QT_MOC_LITERAL(11, 139, 9), // "buildBall"
QT_MOC_LITERAL(12, 149, 13), // "buildCylinder"
QT_MOC_LITERAL(13, 163, 10), // "BaseRotate"
QT_MOC_LITERAL(14, 174, 5), // "angle"
QT_MOC_LITERAL(15, 180, 8), // "J1Rotate"
QT_MOC_LITERAL(16, 189, 8), // "J2Rotate"
QT_MOC_LITERAL(17, 198, 8), // "J3Rotate"
QT_MOC_LITERAL(18, 207, 8), // "J4Rotate"
QT_MOC_LITERAL(19, 216, 8), // "J5Rotate"
QT_MOC_LITERAL(20, 225, 8), // "J6Rotate"
QT_MOC_LITERAL(21, 234, 12), // "hideAxisMode"
QT_MOC_LITERAL(22, 247, 15), // "addMoveLinePath"
QT_MOC_LITERAL(23, 263, 11), // "addLinePath"
QT_MOC_LITERAL(24, 275, 10), // "addArcPath"
QT_MOC_LITERAL(25, 286, 3), // "run"
QT_MOC_LITERAL(26, 290, 14) // "clearPathArray"

    },
    "RobotSimulateWidget\0loadObject\0\0"
    "loadJoint\0removeObject\0removeJoint\0"
    "setRobotParameter\0assembleTool\0"
    "customObject\0buildBox\0std::vector<float>\0"
    "buildBall\0buildCylinder\0BaseRotate\0"
    "angle\0J1Rotate\0J2Rotate\0J3Rotate\0"
    "J4Rotate\0J5Rotate\0J6Rotate\0hideAxisMode\0"
    "addMoveLinePath\0addLinePath\0addArcPath\0"
    "run\0clearPathArray"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_RobotSimulateWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  129,    2, 0x0a /* Public */,
       3,    0,  130,    2, 0x0a /* Public */,
       4,    0,  131,    2, 0x0a /* Public */,
       5,    0,  132,    2, 0x0a /* Public */,
       6,    0,  133,    2, 0x0a /* Public */,
       7,    0,  134,    2, 0x0a /* Public */,
       8,    0,  135,    2, 0x0a /* Public */,
       9,    1,  136,    2, 0x0a /* Public */,
      11,    1,  139,    2, 0x0a /* Public */,
      12,    1,  142,    2, 0x0a /* Public */,
      13,    1,  145,    2, 0x0a /* Public */,
      15,    1,  148,    2, 0x0a /* Public */,
      16,    1,  151,    2, 0x0a /* Public */,
      17,    1,  154,    2, 0x0a /* Public */,
      18,    1,  157,    2, 0x0a /* Public */,
      19,    1,  160,    2, 0x0a /* Public */,
      20,    1,  163,    2, 0x0a /* Public */,
      21,    0,  166,    2, 0x0a /* Public */,
      22,    0,  167,    2, 0x0a /* Public */,
      23,    0,  168,    2, 0x0a /* Public */,
      24,    0,  169,    2, 0x0a /* Public */,
      25,    0,  170,    2, 0x0a /* Public */,
      26,    0,  171,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 10,    2,
    QMetaType::Void, QMetaType::Float,    2,
    QMetaType::Void, 0x80000000 | 10,    2,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void RobotSimulateWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        RobotSimulateWidget *_t = static_cast<RobotSimulateWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->loadObject(); break;
        case 1: _t->loadJoint(); break;
        case 2: _t->removeObject(); break;
        case 3: _t->removeJoint(); break;
        case 4: _t->setRobotParameter(); break;
        case 5: _t->assembleTool(); break;
        case 6: _t->customObject(); break;
        case 7: _t->buildBox((*reinterpret_cast< std::vector<float>(*)>(_a[1]))); break;
        case 8: _t->buildBall((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 9: _t->buildCylinder((*reinterpret_cast< std::vector<float>(*)>(_a[1]))); break;
        case 10: _t->BaseRotate((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->J1Rotate((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->J2Rotate((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->J3Rotate((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->J4Rotate((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->J5Rotate((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->J6Rotate((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->hideAxisMode(); break;
        case 18: _t->addMoveLinePath(); break;
        case 19: _t->addLinePath(); break;
        case 20: _t->addArcPath(); break;
        case 21: _t->run(); break;
        case 22: _t->clearPathArray(); break;
        default: ;
        }
    }
}

const QMetaObject RobotSimulateWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_RobotSimulateWidget.data,
      qt_meta_data_RobotSimulateWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *RobotSimulateWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *RobotSimulateWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_RobotSimulateWidget.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int RobotSimulateWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 23)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 23;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
