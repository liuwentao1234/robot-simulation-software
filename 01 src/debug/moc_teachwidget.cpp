/****************************************************************************
** Meta object code from reading C++ file 'teachwidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../teachwidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'teachwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TeachWidget_t {
    QByteArrayData data[17];
    char stringdata0[249];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TeachWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TeachWidget_t qt_meta_stringdata_TeachWidget = {
    {
QT_MOC_LITERAL(0, 0, 11), // "TeachWidget"
QT_MOC_LITERAL(1, 12, 11), // "moveJButton"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 14), // "moveLineButton"
QT_MOC_LITERAL(4, 40, 13), // "moveArcButton"
QT_MOC_LITERAL(5, 54, 9), // "runButton"
QT_MOC_LITERAL(6, 64, 14), // "clearAllButton"
QT_MOC_LITERAL(7, 79, 10), // "shadowOpen"
QT_MOC_LITERAL(8, 90, 11), // "shadowClose"
QT_MOC_LITERAL(9, 102, 14), // "on_run_clicked"
QT_MOC_LITERAL(10, 117, 16), // "on_moveJ_clicked"
QT_MOC_LITERAL(11, 134, 25), // "on_clearAllButton_clicked"
QT_MOC_LITERAL(12, 160, 19), // "on_moveLine_clicked"
QT_MOC_LITERAL(13, 180, 24), // "on_checkBox_stateChanged"
QT_MOC_LITERAL(14, 205, 4), // "arg1"
QT_MOC_LITERAL(15, 210, 19), // "on_checkBox_clicked"
QT_MOC_LITERAL(16, 230, 18) // "on_moveArc_clicked"

    },
    "TeachWidget\0moveJButton\0\0moveLineButton\0"
    "moveArcButton\0runButton\0clearAllButton\0"
    "shadowOpen\0shadowClose\0on_run_clicked\0"
    "on_moveJ_clicked\0on_clearAllButton_clicked\0"
    "on_moveLine_clicked\0on_checkBox_stateChanged\0"
    "arg1\0on_checkBox_clicked\0on_moveArc_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TeachWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x06 /* Public */,
       3,    0,   85,    2, 0x06 /* Public */,
       4,    0,   86,    2, 0x06 /* Public */,
       5,    0,   87,    2, 0x06 /* Public */,
       6,    0,   88,    2, 0x06 /* Public */,
       7,    0,   89,    2, 0x06 /* Public */,
       8,    0,   90,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    0,   91,    2, 0x08 /* Private */,
      10,    0,   92,    2, 0x08 /* Private */,
      11,    0,   93,    2, 0x08 /* Private */,
      12,    0,   94,    2, 0x08 /* Private */,
      13,    1,   95,    2, 0x08 /* Private */,
      15,    0,   98,    2, 0x08 /* Private */,
      16,    0,   99,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void TeachWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TeachWidget *_t = static_cast<TeachWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->moveJButton(); break;
        case 1: _t->moveLineButton(); break;
        case 2: _t->moveArcButton(); break;
        case 3: _t->runButton(); break;
        case 4: _t->clearAllButton(); break;
        case 5: _t->shadowOpen(); break;
        case 6: _t->shadowClose(); break;
        case 7: _t->on_run_clicked(); break;
        case 8: _t->on_moveJ_clicked(); break;
        case 9: _t->on_clearAllButton_clicked(); break;
        case 10: _t->on_moveLine_clicked(); break;
        case 11: _t->on_checkBox_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_checkBox_clicked(); break;
        case 13: _t->on_moveArc_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (TeachWidget::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TeachWidget::moveJButton)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (TeachWidget::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TeachWidget::moveLineButton)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (TeachWidget::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TeachWidget::moveArcButton)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (TeachWidget::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TeachWidget::runButton)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (TeachWidget::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TeachWidget::clearAllButton)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (TeachWidget::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TeachWidget::shadowOpen)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (TeachWidget::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TeachWidget::shadowClose)) {
                *result = 6;
                return;
            }
        }
    }
}

const QMetaObject TeachWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_TeachWidget.data,
      qt_meta_data_TeachWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TeachWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TeachWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TeachWidget.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int TeachWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void TeachWidget::moveJButton()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void TeachWidget::moveLineButton()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void TeachWidget::moveArcButton()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void TeachWidget::runButton()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void TeachWidget::clearAllButton()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void TeachWidget::shadowOpen()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void TeachWidget::shadowClose()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
