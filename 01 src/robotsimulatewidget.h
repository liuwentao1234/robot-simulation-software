﻿#ifndef ROBOTSIMULATEWIDGET_H
#define ROBOTSIMULATEWIDGET_H
#include <QWidget>
#include <vector>
#include "pathelement.h"        /// 轨迹数组

class OsgWidget;
class TeachWidget;
class SimulateControlWidget;
class QTextEdit;
class QGridLayout;
class IFKinematicsHandle;
class SetRobotParameterDialog;
class CustomObjectDialog;

class RobotSimulateWidget : public QWidget
{
    Q_OBJECT
    public:

        explicit RobotSimulateWidget(QWidget *parent = nullptr);

        virtual ~RobotSimulateWidget();

#if 0
        typedef void(RobotSimulateWidget::*pFunc)(int);        // 控制轴旋转的函数指针类型
        pFunc jointRotate[7];                                  // 控制轴旋转的函数指针数组
#endif

    public slots:

        /** 载入关节/工件 */
        void loadObject();
        void loadJoint();

        /** 移除关节/工件 */
        void removeObject();
        void removeJoint();

        /** 设置机器人参数 */
        void setRobotParameter();

        /** 装配机器人工具 */
        void assembleTool();

        /** 打开自定义工件窗口 */
        void customObject();

        /** 生成盒体工件 */
        void buildBox(std::vector<float>);

        /** 生成球体工件 */
        void buildBall(float);

        /** 生成圆柱体工件 */
        void buildCylinder(std::vector<float>);

        /** 各关节旋转 */
        void BaseRotate(int angle);
        void J1Rotate(int angle);
        void J2Rotate(int angle);
        void J3Rotate(int angle);
        void J4Rotate(int angle);
        void J5Rotate(int angle);
        void J6Rotate(int angle);

        /** 隐藏工件的控制环和控制坐标轴 */
        void hideAxisMode();

        /** 添加空移/直线/圆弧轨迹线 */
        void addMoveLinePath();
        void addLinePath();
        void addArcPath();

        /** 开始轨迹运动 */
        void run();

        /** 清除运动结束后的轨迹和运动残影 */
        void clearPathArray();


    private:

        /** 负责仿真界面的初始化/布局/信号与槽的一些初始化工作 */
        void init();
        void layout();
        void createAction() const;

        /** 输出到日志板上 */
        void displayOnTextEdit(const QString& info) const;

        /** 在textEdit上显示工具末端坐标值 */
        void displayCurrentPositionOnTextEdit();


    private:

        QSharedPointer<IFKinematicsHandle> ifkinematicsHandle;          /// 机器人运动学句柄
        QSharedPointer<QTextEdit> textEdit;                             /// 显示信息的窗口

        QSharedPointer<SimulateControlWidget> simulateControlWidget;    /// 控制台窗口
        QSharedPointer<OsgWidget> osgWidget;                            /// osg渲染的窗口

        QSharedPointer<TeachWidget> teachWidget;                        /// 示教窗口
        QSharedPointer<QGridLayout> gridLayout;                         /// 布局对象是要一直存在的
        QString fileName;

        QSharedPointer<SetRobotParameterDialog> dialog;                 /// 设置机器人参数对话框
        QSharedPointer<CustomObjectDialog> customObjectDialog;          /// 自定义工件参数对话框

        std::vector<PathElement> pathElementArray;                      /// 轨迹数组

};

#endif // ROBOTSIMULATEWIDGET_H
