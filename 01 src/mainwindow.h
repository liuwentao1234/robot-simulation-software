﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}
QT_BEGIN_NAMESPACE
class QAction;
class QMenu;
class QStackedWidget;
class QListWidget;
QT_END_NAMESPACE

class CodeEditWidget;           // 代码编辑界面
class GraphWidget;              // 绘图界面
class RobotSimulateWidget;      // 机器人仿真
class PathSimulateWidget;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

private:
    void createActions();
    void createStatusBar();
    void setLayout();

private:
    Ui::MainWindow *ui;
    QListWidget* listWidget;
    QStackedWidget *stackedWidget;
    QDockWidget* dockWidget;

private:
    CodeEditWidget* codeEditWidget;
    GraphWidget* graphWidget;
    RobotSimulateWidget* robotSimulateWidget;
    PathSimulateWidget* pathSimulateWidget;
};

#endif // MAINWINDOW_H
