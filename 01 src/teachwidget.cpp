﻿#include "teachwidget.h"
#include "ui_teachwidget.h"
#include "QDebug"


TeachWidget::TeachWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TeachWidget)
{
    ui->setupUi(this);
}



TeachWidget::~TeachWidget()
{
    delete ui;
}



void TeachWidget::addItem(const QString & text)
{
    this->ui->listWidget->addItem(text);
}



void TeachWidget::on_run_clicked()
{
    emit runButton();
}



void TeachWidget::on_moveJ_clicked()
{
    emit moveJButton();
}



void TeachWidget::on_clearAllButton_clicked()
{

#ifndef NDEBUG
    qDebug() << "on_clearAllButton_clicked";
#endif

    int cnt = ui->listWidget->count();

    for(int i = 0; i < cnt; ++i) {
        ui->listWidget->takeItem(0);
    }

    emit clearAllButton();

}



void TeachWidget::on_moveLine_clicked()
{  
    emit moveLineButton();
}



void TeachWidget::on_checkBox_clicked()
{

    if (ui->checkBox->isChecked()) {
#ifndef NDEBUG
        qDebug() << " emit shadowOpen()";
#endif
        emit shadowOpen();
    } else {
        emit shadowClose();
    }

}



void TeachWidget::on_checkBox_stateChanged(int)
{

}

void TeachWidget::on_moveArc_clicked()
{
    emit moveArcButton();
}
