﻿#ifndef PATHSIMULATEWIDGET_H
#define PATHSIMULATEWIDGET_H

#include <QWidget>

class DrawPathWidget;
class QTextEdit;
class QPushButton;
class QGridLayout;
class PathSimulateControlWidget;
class PathSimulateWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PathSimulateWidget(QWidget *parent = nullptr);

private:
    void init();
    void layout();
    void createAction();

public slots:
    void loadPath();
    void deletePath();
    void setCurrentText(QString currentText);
    void computer(QVector<double>);


private:
    void displayOnTextEdit(const QString& info) const;

private:
    QSharedPointer<DrawPathWidget> drawPathWidget;
    QSharedPointer<QTextEdit> textEdit;
    QSharedPointer<QPushButton> openBnt;
    QSharedPointer<QGridLayout> gridLayout;
    QSharedPointer<PathSimulateControlWidget> controlWidget;
    QString currentText;

};

#endif // PATHSIMULATEWIDGET_H
